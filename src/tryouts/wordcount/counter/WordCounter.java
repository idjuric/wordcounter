package tryouts.wordcount.counter;

import java.io.IOException;
import java.time.Clock;

public abstract class WordCounter {
	protected String wordsFilePrefix, wordsFileSuffix, version;
	protected int filesCount;
	protected boolean slicingWordsAllowed;
	
	public WordCounter(final String wordsFilePrefix, final String wordsFileSuffix, final int filesCount) {
		this(wordsFilePrefix, wordsFileSuffix, filesCount, true);
	}
	
	public WordCounter(final String wordsFilePrefix, final String wordsFileSuffix, final int filesCount, final boolean slicingWordsAllowed) {
		this.wordsFilePrefix = wordsFilePrefix;
		this.wordsFileSuffix = wordsFileSuffix;
		this.filesCount = filesCount;
		this.slicingWordsAllowed = slicingWordsAllowed;
	}

	public abstract void count(Clock clock, long millisStart) throws InterruptedException, IOException;
	
	protected long checkPoint(final String checkPointId, Clock clock, long previousMillisStart) {
		System.out.println("[" + this.version + " " + Thread.currentThread().getName() + " CHK-" + checkPointId + "] Processing time: " + (clock.millis() - previousMillisStart) + "ms");
		return clock.millis();
	}
}
