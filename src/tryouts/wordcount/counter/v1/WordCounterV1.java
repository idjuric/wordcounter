package tryouts.wordcount.counter.v1;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Clock;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import tryouts.wordcount.counter.WordCounter;

public class WordCounterV1 extends WordCounter {
	
	public WordCounterV1(final String wordsFilePrefix, final String wordsFileSuffix, final int filesCount) {
		super(wordsFilePrefix, wordsFileSuffix, filesCount);
		this.version = "V1";
	}

	public void count(Clock clock, long millisStart) throws InterruptedException {
		long previousMillisStart = millisStart;

		// Create list of text file paths and use it to initialize callables
		List<Callable<Map<String, Long>>> callables = IntStream.range(1, filesCount + 1)
			.mapToObj(i -> new File(wordsFilePrefix + i + wordsFileSuffix).toPath())
			.unordered()
			.distinct()
			//.map(filePath -> new WordCounterCallableInMemory(filePath))
			.map(filePath -> new WordCounterCallableLineByLine(filePath)) // typically up to 8x faster than WordCounterCallableInMemory!
			.collect(Collectors.toList());

		// Check point 1
		previousMillisStart = checkPoint("1", clock, previousMillisStart);
		
		// Initiate executor service
		// newWorkStealingPool() is some 10% faster than newCachedThreadPool() or newFixedThreadPool(10), and more than twice as fast as SingleThreadExecutor()
		ExecutorService executor = Executors.newWorkStealingPool();
		
		// Invoke word counting callables
		List<Map<String, Long>> wordCountMapsList = executor.invokeAll(callables)
	    	.parallelStream()
	    	.map(future -> {
	    		try {
		            return future.get();
		        }
		        catch (Exception e) {
		            throw new IllegalStateException(e);
		        }
		    })
	    	.collect(Collectors.toList());
		executor.shutdown();
		
		// Checkpoint 2
		previousMillisStart = checkPoint("2", clock, previousMillisStart);

		// Combine all word frequency maps
		Map<String, Long> combinedWordCountMap = new HashMap<String, Long>(); // TODO set initialCapacity
		for (Map<String, Long> wordCountMap : wordCountMapsList)
			combinedWordCountMap = Stream.concat(combinedWordCountMap.entrySet().stream(), wordCountMap.entrySet().stream())
				    .collect(Collectors.toMap(entry -> entry.getKey(), entry -> entry.getValue(), Long::sum));
		
		// Checkpoint 3
		previousMillisStart = checkPoint("3", clock, previousMillisStart);

		// Sort map entries and print them
		combinedWordCountMap.entrySet()
			.stream()
			.sorted(Map.Entry.<String, Long>comparingByValue().reversed())
			.limit(100)
			.forEach(System.out::println);
		
		// Checkpoint 4
		previousMillisStart = checkPoint("4", clock, previousMillisStart);
	}
}

/**
 * A callable that reads entire file into RAM first.
 * Use WordCounterCallableLineByLine instead.
 * 
 * @author djurici
 * @deprecated
 */
class WordCounterCallableInMemory implements Callable<Map<String, Long>> {
	private Path filePath;
	
	WordCounterCallableInMemory (final Path filePath) {
		this.filePath = filePath;
	}
	
	@Override
    public Map<String, Long> call() throws Exception {
        System.out.println("In WordCounterCallableInMemory(\"" + filePath + "\").call()");
        
	    return Arrays.stream(new String(Files.readAllBytes(filePath), StandardCharsets.UTF_8).split("\\W+"))
		    	.map(String::toUpperCase)
		    	.collect(Collectors.toConcurrentMap(w -> w, w -> 1L, Long::sum));
    }
}

/**
 * A callable that reads entire file line by line by applying Java NIO library.
 * 
 * @author djurici
 *
 */
class WordCounterCallableLineByLine implements Callable<Map<String, Long>> {
	private Path filePath;
	private static final Pattern PATTERN = Pattern.compile("\\P{IsAlphabetic}+");
	
	WordCounterCallableLineByLine (final Path filePath) {
		this.filePath = filePath;
	}
	
	@Override
    public Map<String, Long> call() throws Exception {
        System.out.println("In WordCounterCallableLineByLine(\"" + filePath + "\").call()");

        return Files.lines(filePath)
		    	.map(String::toUpperCase)
	    		.map(w -> PATTERN.split(w))
	    		.flatMap(Arrays::stream)
		    	.collect(Collectors.toConcurrentMap(w -> w, w -> 1L, Long::sum));
        }
}

