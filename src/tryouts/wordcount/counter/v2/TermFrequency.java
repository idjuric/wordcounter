package tryouts.wordcount.counter.v2;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Concurrent map whose keys are the words (terms) encountered in the files while 
 * associated values represent number of occurrences.
 * 
 * @author djurici
 *
 */
public class TermFrequency {
    private final Map<String, AtomicInteger> tfMap;
    
    public TermFrequency(int parallelismLevel) {
        this.tfMap = new ConcurrentHashMap<String, AtomicInteger>(8192, 0.8f, parallelismLevel);
    }
    
    public int getSize() {
        return tfMap.size();
    }
    
    public Map<String, AtomicInteger> getTfMap() {
    	return this.tfMap;
    }
    
    public void add(String term, int count) {
        AtomicInteger termFrequency = tfMap.get(term);
        if (termFrequency != null) {
        	termFrequency.addAndGet(count);
        } else {
        	termFrequency = ((ConcurrentMap<String, AtomicInteger>) tfMap).putIfAbsent(term, new AtomicInteger(count));
            if (termFrequency != null) termFrequency.addAndGet(count);
        }
    }
    
    public void increment(String term) {
    	AtomicInteger termFrequency = tfMap.get(term);
    	if (termFrequency != null) {
    		termFrequency.incrementAndGet();
    	} else {
    		termFrequency = ((ConcurrentMap<String, AtomicInteger>) tfMap).putIfAbsent(term, new AtomicInteger(1));
    		if (termFrequency != null) termFrequency.incrementAndGet();
    	}
    }
    
    public void set(String term, int count) {
    	AtomicInteger termFrequency = tfMap.get(term);
    	if (termFrequency != null) {
    		termFrequency.set(count);
    	} else {
    		termFrequency = ((ConcurrentMap<String, AtomicInteger>) tfMap).putIfAbsent(term, new AtomicInteger(count));
    		if (termFrequency != null) termFrequency.set(count);
    	}
    }
}
