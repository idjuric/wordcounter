/**
 * 
 */
package tryouts.wordcount.counter.v2;

import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ForkJoinTask;
//import java.time.Clock;
import java.util.concurrent.RecursiveAction;

import tryouts.wordcount.ConcurrencyUtils;

/**
 * Fork/join task.
 * 
 * @author djurici
 *
 */
public class CountTermsTask extends RecursiveAction {
	private static final long serialVersionUID = 1L;
	//private static final Pattern SPLITTING_PATTERN = Pattern.compile("\\P{IsAlphabetic}+");
	//private static final Splitter SPLITTER = Splitter.on(SPLITTING_PATTERN).trimResults();
	//private static final Clock clock = Clock.systemDefaultZone();
	
	private TermFrequency tf;
	private FileChannel inChannel;
	private MappedByteBuffer buffer;
	private long start, end, maxBufferSize;
	private boolean slicingWordsAllowed;
	
	private CharBuffer charBuffer;
	private int bufferLength;
	CountTermsTask task1, task2;
	long middle;

	public CountTermsTask(TermFrequency tf, FileChannel inChannel, long start, long end, long maxBufferSize, final boolean slicingWordsAllowed) {
		//System.out.println("[" + Thread.currentThread().getId() + " constructor()] start: " + start + ", end: " + end + ", inChannel:" + inChannel);
		this.tf = tf;
		this.inChannel = inChannel;
		this.start = start;
		this.end = end;
		this.maxBufferSize = maxBufferSize;
		this.slicingWordsAllowed = slicingWordsAllowed;
	}
	
	/* (non-Javadoc)
	 * @see java.util.concurrent.RecursiveAction#compute()
	 */
	@Override
	protected void compute() {
		//System.out.println("[" + Thread.currentThread().getId() + " compute()] start: " + start + ", end: " + end + ", (end - start) > maxBufferSize:" + ((end - start) > maxBufferSize));
		ConcurrencyUtils.dumpForkJoinPoolStatsShort(ForkJoinTask.getPool(), "compute() START");

		if ((end - start) > maxBufferSize) {
			//System.out.println("[" + Thread.currentThread().getId() + " compute()] A");
			//long previousMillisStart = clock.millis();

			// Divide task into two parts of ideally equal (or at least similar) size
			middle = (start + end) / 2;
	        int offset = 0;
			
			// [PERFORMANCE WARNING] The following code reduces performance by half!
			// Tune the middle index value so parts of single word would not end up in two different slices
			// Assume that maxBufferSize is always (much) greater than the maximal word length
	        if (!slicingWordsAllowed) {
				try {
					buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, middle, end - middle);
			        buffer.load();
			        charBuffer = StandardCharsets.UTF_8.decode(buffer).asReadOnlyBuffer();
			        bufferLength = charBuffer.length();
			        
			        while (offset < bufferLength && Character.isAlphabetic(charBuffer.charAt(offset))) offset++;
					
					// Check point 1A
					//previousMillisStart = taskCheckPoint("1A", clock, previousMillisStart);
				} catch (IOException e) {
					e.printStackTrace();
				}
	        }
	        
	        // Fork 'em
			task1 = new CountTermsTask(tf, inChannel, start, middle + offset, maxBufferSize, slicingWordsAllowed);
			task2 = new CountTermsTask(tf, inChannel, middle + offset + 1, end, maxBufferSize, slicingWordsAllowed);
			invokeAll(task1, task2);
			
			// Check point 2A
			//previousMillisStart = taskCheckPoint("2A", clock, previousMillisStart);
			
		} else {
			//System.out.println("[" + Thread.currentThread().getId() + " compute()] B");
			//long previousMillisStart = clock.millis();

			// Perform task
			try {
				
				// Check point 1B
				//previousMillisStart = taskCheckPoint("1B", clock, previousMillisStart);

				buffer = inChannel.map(FileChannel.MapMode.READ_ONLY, start, end - start);
		        buffer.load();
		        charBuffer = StandardCharsets.UTF_8.decode(buffer).asReadOnlyBuffer();
		        bufferLength = charBuffer.length();
		        
				// Check point 2B
				//previousMillisStart = taskCheckPoint("2B", clock, previousMillisStart);

		        // VARIANT 1 :: Splitter is very slow!
		        //for (String term : SPLITTER.split(charBuffer.toString())) tf.add(term, 1);
		        
		        // VARIANT 2 ::  Working with characters directly
		        int termStartIndex, termEndIndex, i = 0;
		        while (i < bufferLength) {
		            while (i < bufferLength && !Character.isAlphabetic(charBuffer.charAt(i))) i++;
		            termStartIndex = i;
		            while (i < bufferLength && Character.isAlphabetic(charBuffer.charAt(i))) i++;
		            termEndIndex = i;
		            if (termStartIndex != termEndIndex) {
		                //tf.add(charBuffer.subSequence(termStartIndex, termEndIndex).toString(), 1);
		                tf.increment(charBuffer.subSequence(termStartIndex, termEndIndex).toString());
		            }
		        }
		        //buffer.clear();
				
		        // Check point 3B
				//previousMillisStart = taskCheckPoint("3B", clock, previousMillisStart);
			} catch (IOException e) {
				e.printStackTrace();
			}
			ConcurrencyUtils.dumpForkJoinPoolStatsShort(ForkJoinTask.getPool(), "compute() END  ");
		}
	}
	
	/*
	private long taskCheckPoint(final String checkPointId, Clock clock, long previousMillisStart) {
		System.out.println("[" + Thread.currentThread().getId() + " CHK-" + checkPointId + "] Processing time: " + (clock.millis() - previousMillisStart) + "ms");
		return clock.millis();
	}
	*/
}
