package tryouts.wordcount.counter.v2;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.time.Clock;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import tryouts.wordcount.ConcurrencyUtils;
import tryouts.wordcount.counter.WordCounter;


/**
 * Some pieces of code might have used Guava library.
 * 
 * @author djurici
 *
 */
public class WordCounterV2 extends WordCounter {
	//private static final int PARALLELISM_LEVEL = Runtime.getRuntime().availableProcessors();
	private static final int PARALLELISM_LEVEL = 30; // optimal values: 30 with MAX_BUFFER_SIZE = 256 * 1024l
	private static final long MAX_BUFFER_SIZE = 256 * 1024l; // optimal values: 256 * 1024l with PARALLELISM_LEVEL = 30
	
	public WordCounterV2(final String wordsFilePrefix, final String wordsFileSuffix, final int filesCount, final boolean slicingWordsAllowed) {
		super(wordsFilePrefix, wordsFileSuffix, filesCount, slicingWordsAllowed);
		this.version = "V2";
	}

	@Override
	public void count(Clock clock, long millisStart) throws InterruptedException, IOException {
		long previousMillisStart = millisStart;

		TermFrequency tf = new TermFrequency(PARALLELISM_LEVEL);	

		Map<Integer, RandomAccessFile> files = new HashMap<Integer, RandomAccessFile>(filesCount);
		Map<Integer, FileChannel> fileChannels = new HashMap<Integer, FileChannel>(filesCount);
		RandomAccessFile termsFile = null;
		FileChannel inChannel = null;

		try {
			
			// STEP 1 :: Open file channels
			for (int i = 1; i <= filesCount; i++) {
				termsFile = new RandomAccessFile(wordsFilePrefix + i + wordsFileSuffix, "r");
		        inChannel = termsFile.getChannel();
				files.put(i, termsFile);
				fileChannels.put(i, inChannel);
			}
			
			// Check point 1
			previousMillisStart = checkPoint("1", clock, previousMillisStart);
			
			// STEP 2 :: Obtain term frequencies
			ForkJoinPool pool = ForkJoinPool.commonPool();
			ConcurrencyUtils.dumpForkJoinPoolStats(pool, "1");

			// BLOK 1
			/*
			IntStream.range(1, filesCount)
				.parallel()
				.peek(n -> System.out.println(Thread.currentThread().getName() + ": Number " + n))
				.mapToObj(i -> {
					try {
						return new CountTermsTask(tf, fileChannels.get(i), 0, fileChannels.get(i).size(), MAX_BUFFER_SIZE, slicingWordsAllowed);
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				})
				.peek(task -> System.out.println(Thread.currentThread().getName() + ": Task " + task))
				.forEach(task -> {
					pool.execute(task);
					task.join();
				});
			*/

			// BLOK 2
			/*
			IntStream.range(1, filesCount)
				.peek(n -> System.out.println(Thread.currentThread().getName() + ": Number " + n))
				.mapToObj(i -> {
					try {
						return new CountTermsTask(tf, fileChannels.get(i), 0, fileChannels.get(i).size(), MAX_BUFFER_SIZE, slicingWordsAllowed);
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				})
				.peek(task -> System.out.println(Thread.currentThread().getName() + ": Task " + task))
				.map(pool::submit)
				.forEach(ForkJoinTask::join);
			*/
			
			// BLOK 3
            List<ForkJoinTask<Void>> futures = IntStream.range(1, filesCount)
            	.peek(n -> System.out.println(Thread.currentThread().getName() + ": Number " + n))
                .mapToObj(i -> {
                    try {
                        return new CountTermsTask(tf, fileChannels.get(i), 0, fileChannels.get(i).size(), MAX_BUFFER_SIZE, slicingWordsAllowed);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
				.peek(task -> System.out.println(Thread.currentThread().getName() + ": Task " + task))
                .map(pool::submit)
                .collect(Collectors.toList());
            futures.forEach(ForkJoinTask::join);
			
			ConcurrencyUtils.dumpForkJoinPoolStats(pool, "2");
	        pool.shutdown();
			ConcurrencyUtils.dumpForkJoinPoolStats(pool, "3");
		
			// Check point 2
			previousMillisStart = checkPoint("2", clock, previousMillisStart);
	    } finally {
			for (int i = 1; i <= filesCount; i++) {
		        termsFile = files.get(i);
				inChannel = fileChannels.get(i);
		        if (inChannel != null) inChannel.close();
		        if (termsFile != null) termsFile.close();
			}
	    }

		// STEP 3 :: Sort obtained results and print 10 most frequent words
        System.out.println("Term frequencies:");
		tf.getTfMap().entrySet()
			.stream()
			.sorted(Map.Entry.<String, AtomicInteger>comparingByValue(new Comparator<AtomicInteger>() {
				@Override
				public int compare(AtomicInteger a1, AtomicInteger a2) {
					return a1.get() - a2.get();
				}
			}).reversed())
			.limit(10)
			.forEach(System.out::println);
		
		// Check point 3
		previousMillisStart = checkPoint("3", clock, previousMillisStart);
	}
}
