package tryouts.wordcount;

import java.io.IOException;
import java.time.Clock;

import tryouts.wordcount.counter.WordCounter;
import tryouts.wordcount.counter.v1.WordCounterV1;
import tryouts.wordcount.counter.v2.WordCounterV2;

/**
 * @author djurici
 *
 */
@SuppressWarnings("unused")
public class WordCounterApp {
	private static final String WORDS_FILE_PREFIX = "c:/work/eval/sd/testbed/wordcount/big";
	private static final String WORDS_FILE_SUFFIX = ".txt";
	private static final int FILES_COUNT = 30; // up to 30
	
	public static void main(String[] args) throws IOException, InterruptedException {
		WordCounter counter;
		
		// Wait 20 seconds for monitoring tool to start monitoring
		//System.out.println("Waiting for monitoring tool to connect...");
		//Thread.sleep(20000);
		
		// Start stopwatch
		Clock clock = Clock.systemDefaultZone();
		long millisStart = clock.millis();

		// Different counter algorithms chooser
		counter =
			//new WordCounterV1(WORDS_FILE_PREFIX, WORDS_FILE_SUFFIX, FILES_COUNT); // takes cca. 6.5-7 seconds
			new WordCounterV2(WORDS_FILE_PREFIX, WORDS_FILE_SUFFIX, FILES_COUNT, true); // takes cca. 4.2 seconds (even 2.9 when slicing through words is allowed!)

		counter.count(clock, millisStart);

		// Stop stopwatch
		System.out.println("Processing time: " + (clock.millis() - millisStart) + " ms");
	}
}