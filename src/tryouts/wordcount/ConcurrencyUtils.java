package tryouts.wordcount;

import java.util.concurrent.ForkJoinPool;

public class ConcurrencyUtils {

	/**
	 * Displays current stats of the supplied ForkJoinPool object.
	 * 
	 * @param pool
	 * @param label
	 */
	public static void dumpForkJoinPoolStats(ForkJoinPool pool, final String label) {
		
		/*
		 * getParallelism(): This method returns the desired level of parallelism established for the pool.
		 * getPoolSize(): This method returns the number of threads in the pool.
		 * getActiveThreadCount(): This method returns the number of threads in the pool that are currently executing tasks.
		 * getRunningThreadCount(): This method returns the number of threads that are not waiting for the finalization of their child tasks.
		 * getQueuedSubmissionCount(): This method returns the number of tasks that have been submitted to a pool that haven�t started their execution yet.
		 * getQueuedTaskCount(): This method returns the number of tasks in the work-stealing queues of this pool.
		 * hasQueuedSubmissions(): This method returns true if there are tasks that have been submitted to the pool that haven�t started their execution yet. 
		 *                         It returns false otherwise.
		 * getStealCount(): This method returns the number of times the Fork/Join pool has executed the work-stealing algorithm.
		 * isTerminated(): This method returns true if the Fork/Join pool has finished its execution. It returns false otherwise.
		 */
		
		System.out.println("[" + label + "]**********************");
		System.out.println("Parallelism: "+pool.getParallelism());
		System.out.println("Pool Size: " + pool.getPoolSize());
		System.out.println("Active Thread Count: " + pool.getActiveThreadCount());
		System.out.println("Running Thread Count: " + pool.getRunningThreadCount());
		System.out.println("Queued Submission: " + pool.getQueuedSubmissionCount());
		System.out.println("Queued Tasks: " + pool.getQueuedTaskCount());
		System.out.println("Queued Submissions: " + pool.hasQueuedSubmissions());
		System.out.println("Steal Count: " + pool.getStealCount());
		System.out.println("Terminated : " + pool.isTerminated());
		System.out.println("**********************");
	}
	
	/**
	 * Displays current stats of the supplied ForkJoinPool object - less verbose version.
	 * 
	 * @param pool
	 * @param label
	 */
	public static void dumpForkJoinPoolStatsShort(ForkJoinPool pool, final String label) {
		
		/*
		 * getParallelism(): This method returns the desired level of parallelism established for the pool.
		 * getPoolSize(): This method returns the number of threads in the pool.
		 * getActiveThreadCount(): This method returns the number of threads in the pool that are currently executing tasks.
		 * getRunningThreadCount(): This method returns the number of threads that are not waiting for the finalization of their child tasks.
		 * getQueuedSubmissionCount(): This method returns the number of tasks that have been submitted to a pool that haven�t started their execution yet.
		 * getQueuedTaskCount(): This method returns the number of tasks in the work-stealing queues of this pool.
		 * hasQueuedSubmissions(): This method returns true if there are tasks that have been submitted to the pool that haven�t started their execution yet. 
		 *                         It returns false otherwise.
		 * getStealCount(): This method returns the number of times the Fork/Join pool has executed the work-stealing algorithm.
		 * isTerminated(): This method returns true if the Fork/Join pool has finished its execution. It returns false otherwise.
		 */
		String msg = new StringBuffer(300)
				.append("[" + label)
				//.append(" " + pool.toString())
				.append(" " + Thread.currentThread().getName())
				.append(" P:" + (pool != null ? ("" + pool.getParallelism()) : "-"))
				.append(" PS:" + (pool != null ? ("" + pool.getPoolSize()) : "-"))
				.append(" threads A/R:" + (pool != null ? (pool.getActiveThreadCount() + "/" + pool.getRunningThreadCount()) : "-/-"))
				.append(" queued S/T:" + (pool != null ? (pool.getQueuedSubmissionCount() + "/" + pool.getQueuedTaskCount()) : "-/-"))
				.append(" steals:" + (pool != null ? ("" + pool.getStealCount()) : "-"))
				.append(" terminated:" + (pool != null ? ("" + pool.isTerminated()) : "-") + "]")
				.toString();
		System.out.println(msg);
	}
}
